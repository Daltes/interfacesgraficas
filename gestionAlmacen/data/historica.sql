CREATE TABLE Historica(
	clientes varchar(6),
	proveedores varchar(6),
	articulos varchar(6),
	unidades int,
	fecha date,
	FOREIGN KEY (clientes) REFERENCES Clientes(codigo),
	FOREIGN KEY (proveedores) REFERENCES Proveedores(codigo),
	FOREIGN KEY (articulos) REFERENCES Articulos(codigo)
);