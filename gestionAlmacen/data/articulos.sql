CREATE TABLE Articulos(
	codigo varchar(6) primary key,
	descripcion varchar(25),
	stock float,
	stock_minimo float,
	precio_compra float,
	precio_venta float
);


INSERT INTO Articulos VALUES 
	("000001","Tornillos Plano",50,10,0.10,0.15),
	("000002","Destornillador Plano",30,5,7,10),
	("000003","Manzana",40,5,1,1.5),
	("000004","Bolbillas",30,5,2,4),
	("000005","Destornillador Estrella",30,5,7,10),
	("000006","Tornillos Estrella",50,10,0.10,0.15);