CREATE TABLE Proveedores (
	codigo varchar(6) NOT NULL,
	nif varchar(9),
	apellidos varchar(35),
	nombre varchar(15),
	domicilio varchar(40),
	codigo_postal varchar(5),
	localidad varchar(20),
	telefono varchar(9),
	movil varchar(9),
	fax varchar(9),
	email varchar(20),
	total_ventas float,

    PRIMARY KEY (codigo)
);


INSERT INTO Proveedores VALUES 
	("000001","11111111A","García","Alex","DomicilioUno","11111","LocalidadUno","111111111","111111111","111111111","correo@uno.com",0),
	("000002","22222222B","Maeso","Sergio","DomicilioDos","22222","LocalidadDos","222222222","222222222","222222222","correo@dos.com",0),
	("000003","33333333C","López","Christopher","DomicilioTres","33333","LocalidadTres","333333333","333333333","333333333","correo@tres.com",0);