/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionAlmacen;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

/**
 *
 * @author dany-
 */
class MiDocumento extends PlainDocument {

    // Declaracion de atributos
    private String patron;
    private int maximoLongitud;
    private boolean aceptarPatron;

    public int getMaximoLongitud() {
        return maximoLongitud;
    }

    public void setMaximoLongitud(int maximoLongitud) {
        this.maximoLongitud = maximoLongitud;
    }

    // Fin de declaracion de atributos
    public String getPatron() {
        return patron;
    }

    public void setPatron(String patron) {
        this.patron = patron;
    }

    /**
     * Constructor
     *
     * @param limite int que establece los caracteres maximos que admite
     * @param patron String con el patron para el matches, si es null se aceptan
     * todos los carateres
     */
    MiDocumento(String patron, int maximoLongitud) {
        super();
        // Si el patron es null se establece que se acepta todos los caracteres
        if (patron == null) {
            this.patron = ".*";
        } else {
            this.patron = patron;
        }

        this.maximoLongitud = maximoLongitud;
        this.aceptarPatron = false;
    }

    MiDocumento(String patron, int maximoLongitud, boolean aceptarPatron) {
        super();
        // Si el patron es null se establece que se acepta todos los caracteres
        if (patron == null) {
            this.patron = ".*";
        } else {
            this.patron = patron;
        }

        this.maximoLongitud = maximoLongitud;
        this.aceptarPatron = aceptarPatron;
    }

    /**
     * Metodo que se sobreescribe para añadir un string al documento
     *
     * @param offset int que establece la posicion en la que se introducira el
     * nuevo string en el documento
     * @param str String que se quiere añadir al documento
     * @param attr AttributeSet que se desea asociar al String
     * @throws BadLocationException Exception que indica una mala localizacion
     */
    @Override
    public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
        //Si el str es null no se escribe nada
        if (str == null) {
            return;
        }

        // Se recoge el texto completo del documento
        String textoCompleto = getText(0, getLength()) + str;

        // Si el texto completo del documento coincide con el patron se escribe.
        if (textoCompleto.length() <= maximoLongitud) {

            if (aceptarPatron && !str.matches(patron)) {
                return;
            }

            super.insertString(offset, str, attr);
        }
    }

    /**
     * Tranforma un objeto Document en MiDocumento
     *
     * @param document
     * @return
     */
    public static MiDocumento valueOf(Document document) {
        if (document == null) {
            throw new NullPointerException("Document es null");
        }
        return (MiDocumento) document;
    }

    /**
     * Establece un texto sin pasar por el matches, sustituyendo el texto
     * anterior
     *
     * @param texto String que se desea añadir
     */
    public void establecerTextoCompleto(String texto) {
        try {
            // Se elimina el texto que hubise anteriormente en el documento
            remove(0, getLength());
            // Se añade el nuevo texto
            super.insertString(getLength(), texto, null);
        } catch (BadLocationException ex) {
            System.err.println("Fallo en establecerTextoCompleto");
        }
    }

    public boolean comprobarTexto() {
        try {
            return getText(0, getLength()).matches(patron);
        } catch (BadLocationException ex) {
            Logger.getLogger(MiDocumento.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

}
