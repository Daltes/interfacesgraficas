/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionAlmacen;

import java.sql.Date;


/**
 *
 * @author dany-
 */
public class Historica {

    private String cliente;
    private String proveedor;
    private String articulo;
    private int unidades;
    private Date fecha;

    public Historica(String cliente, String proveedor, String articulo, int unidades, Date fecha) {
        this.cliente = cliente;
        this.proveedor = proveedor;
        this.articulo = articulo;
        this.unidades = unidades;
        this.fecha = fecha;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getArticulo() {
        return articulo;
    }

    public void setArticulo(String articulo) {
        this.articulo = articulo;
    }

    public int getUnidades() {
        return unidades;
    }

    public void setUnidades(int unidades) {
        this.unidades = unidades;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

}
