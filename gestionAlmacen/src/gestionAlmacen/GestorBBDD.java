/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionAlmacen;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author dany-
 */
public class GestorBBDD {

    private Connection con;
    private Statement stBusqueda;

    public GestorBBDD() {
        super();
    }

    /**
     * Establece una conexion con la bbdd
     *
     * @throws Exception
     */
    void conectarBD() throws Exception {

        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

        con = DriverManager.getConnection("jdbc:mysql://172.30.104.111/almacen?serverTimezone=UTC", "root", "root");

        stBusqueda = con.createStatement();
    }

    /**
     * Activa y desactiva el autocommit
     *
     * @param estaActivado boolean que indica si se activa o desactiva
     * @throws Exception
     */
    public void estadoAutoCommit(boolean estaActivado) throws Exception {
        con.setAutoCommit(estaActivado);
    }

    /**
     * Realiza un commit
     *
     * @throws java.lang.Exception
     */
    public void hacerCommit() throws Exception {
        con.commit();
    }

    /**
     * Realiza un rollback
     *
     * @throws java.lang.Exception
     */
    public void hacerRollback() throws Exception {
        con.rollback();
    }

    /**
     * Cierra la conexion con la bbdd
     *
     * @throws Exception
     */
    void desconectarBD() throws Exception {
        stBusqueda.close();
        con.close();
    }

    public Cliente realizarConsultaClientes(String codigo) throws Exception {
        return realizarConsultaClientes(codigo, true);
    }

    /**
     * Realiza una busqueda por el codigo
     *
     * @param codigo String que contiene el codigo que se desea buscar
     * @param esCliente Boolean que indica si es un cliente o proveedor
     * @return String con los datos que ha encontrado, null si no encuentra nada
     * @throws java.lang.Exception Se lanzan las excepciones para controlarlas
     * desde la interfaz
     */
    public Cliente realizarConsultaClientes(String codigo, boolean esCliente) throws Exception {
        String nombreTabla;

        if (esCliente) {
            nombreTabla = "Clientes";
        } else {
            nombreTabla = "Proveedores";
        }
        String sqlBusqueda = "SELECT * FROM " + nombreTabla + " WHERE codigo = '" + codigo + "'";
        String resultadoBusqueda = null;

        ResultSet rs;
        rs = stBusqueda.executeQuery(sqlBusqueda);
        Cliente cliente = null;
        while (rs.next()) {

            cliente = new Cliente(rs.getString("codigo"), rs.getString("nif"),
                    rs.getString("apellidos"), rs.getString("nombre"), rs.getString("domicilio"), rs.getString("codigo_postal"),
                    rs.getString("localidad"), rs.getString("telefono"),
                    rs.getString("movil"), rs.getString("fax"), rs.getString("email"), rs.getDouble("total_ventas"));
        }
        rs.close();
        return cliente;
    }

    /**
     * Guarda en la tabla los datos que se le pasa por parametro. Tiene que
     * tener la estructura:
     * Código;NIF;Apellidos;Nombre;Domicilio;CódigoPostal;Localidad;Teléfono;Móvil;Fax;Email
     *
     * @param datos String en el que se almacenan los datos
     * @throws java.lang.Exception Se lanzan las excepciones para controlarlas
     * desde la interfaz
     */
    public void guardarDatosCliente(String datos) throws Exception {
        String[] datosSeparados = datos.split(";");
        String sqlGuardar = "INSERT INTO Clientes VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement psGuardar;
        psGuardar = con.prepareStatement(sqlGuardar);

        for (int i = 1; i <= 12; i++) {
            psGuardar.setString(i, datosSeparados[i - 1]);
        }

        psGuardar.executeUpdate();

        psGuardar.close();
    }

    /**
     * Borra el elemento de la tabla
     *
     * @param codigo String que contiene el codigo del elemento que se desea
     * cerrar
     * @throws java.lang.Exception Se lanzan las excepciones para controlarlas
     * desde la interfaz
     */
    public void eliminarDatosCliente(String codigo) throws Exception {
        String sqlBorrar = "DELETE FROM Clientes WHERE codigo = '" + codigo + "'";
        stBusqueda.executeUpdate(sqlBorrar);
    }

    /**
     * Modifica un elemento de la tabla. Los datos tienen que tener el formato:
     * Código;NIF;Apellidos;Nombre;Domicilio;CódigoPostal;Localidad;Teléfono;Móvil;Fax;Email
     *
     * @param datos String que contiene los datos modificados
     * @throws java.lang.Exception Se lanzan las excepciones para controlarlas
     * desde la interfaz
     */
    public void modificarDatosCliente(String datos) throws Exception {
        String sqlModificar = "UPDATE Clientes SET nif=?,apellidos=?,nombre=?,"
                + "domicilio=?,codigo_postal=?,localidad=?,telefono=?,movil=?,"
                + "fax=?,email=?,total_ventas=? WHERE codigo = ?";
        String[] datosSeparados = datos.split(";");
        PreparedStatement psGuardar;
        psGuardar = con.prepareStatement(sqlModificar);

        for (int i = 1; i <= 12; i++) {
            if (i != 12) {
                psGuardar.setString(i, datosSeparados[i]);
            } else {
                psGuardar.setString(12, datosSeparados[0]);
            }
        }

        psGuardar.executeUpdate();
        psGuardar.close();
    }

    /**
     * Genera un JasperViewer que contiene un listado de la base de datos
     *
     * @param minimo String con el código mínimo para la sentencia SQL
     * @param maximo String con el código máximo para la sentencia SQL
     * @return JasperViewer con el informe
     * @throws Exception Se lanzan las excepciones para controlarlas desde la
     * interfaz
     */
    public JasperViewer ejecutarInforme(String minimo, String maximo) throws Exception {

        JasperViewer vistaInforme;
        /* Creamos una cadena que contendrá la ruta completa donde está
         * almacenado el archivo report1.jasper. */
        String archivoJasper = System.getProperty("user.dir") + ("/dist/report2.jasper");
        if (archivoJasper == null) {
            System.out.println("El archivo report1.jasper no está en /dist.");
        }
        // Se crea un objeto para cargar el informe.
        JasperReport informeCargado;
        informeCargado = (JasperReport) JRLoader.loadObject(new File(archivoJasper));

        /* El parámetro del informe toma valor del parámetro que recibe este
         * método, que es el contenido de la caja de texto en la que se introduce
         * el total. */
        Map parametro = new HashMap();
        parametro.put("codigo_minimo", minimo);
        parametro.put("codigo_maximo", maximo);
        /* Se crea un objeto de tipo JasperPrint que contendrá el informe
         * cargado previamente con el filtrado del parámetro definido y con
         * la conexión a la base de datos. */
        JasperPrint informe;
        informe = JasperFillManager.fillReport(informeCargado, parametro, con);
        /* Se asigna valor al objeto JasperViewer que devuelve este método con
         * el informe almacenado previamente en el objeto JasperPrint. El valor
         * false del constructor indica que, al cerrar la vista previa, la
         * aplicación desde la que se ha llamado a esta vista previa continuará
         * ejecutándose. */
        vistaInforme = new JasperViewer(informe, false);
        vistaInforme.setTitle("Lista Cliente");

        return vistaInforme;
    }

    /**
     * Genera un informe con un gráfico de queso
     *
     * @return JasperViewer que contiene el informe
     * @throws JRException Se lanzan las excepciones para controlarlas desde la
     * interfaz
     */
    public JasperViewer ejecutarGrafico() throws JRException {
        JasperViewer vistaInforme;
        JasperReport informeCargado = (JasperReport) JRLoader.loadObject(new File(System.getProperty("user.dir") + "/dist/reportGrafico.jasper"));
        Map parametro = new HashMap();
        JasperPrint informe = JasperFillManager.fillReport(informeCargado, parametro, con);
        vistaInforme = new JasperViewer(informe, false);
        vistaInforme.setTitle("Gráfico Código Postal");
        return vistaInforme;
    }

    /**
     * Lista el nombre de columnas que se usaran para la tabla
     *
     * @return String [] Contiene los nombres
     */
    public String[] listarNombreColumnasArticulo() {
        String[] nombres = {"Código", "Descripción", "Stock", "Precio"};
        return nombres;
    }

    /**
     * Devuelve una matriz con los datos de la tabla de los campos
     * codigo,descripcion,stock,precio_venta
     *
     * @param esVenta boolean Indica si se usa precio_venta o precio_compra.
     * TRUE precio_venta, FALSE precio_compra
     * @return String [][] matriz con los datos
     * @throws Exception
     */
    public String[][] listarTablaArticulosMatriz(boolean esVenta) throws Exception {
        String precio;

        if (esVenta) {
            precio = "precio_venta";
        } else {
            precio = "precio_compra";
        }

        String sqlBusqueda = "SELECT codigo,descripcion,stock," + precio + " FROM Articulos";

        ResultSet rs;
        rs = stBusqueda.executeQuery(sqlBusqueda);

        ArrayList<String> fila = new ArrayList<>();

        while (rs.next()) {
            String filaTemp = rs.getString("codigo") + ";" + rs.getString("descripcion") + ";" + rs.getString("stock") + ";" + rs.getString(precio);
            fila.add(filaTemp);
        }
        rs.close();

        // Se crea la matriz
        String[][] resultado = new String[fila.size()][4];

        for (int i = 0; i < resultado.length; i++) {

            String[] filaTem = fila.get(i).split(";");

            resultado[i][0] = filaTem[0];
            resultado[i][1] = filaTem[1];
            resultado[i][2] = filaTem[2];
            resultado[i][3] = filaTem[3];

        }

        return resultado;
    }

    /**
     * Realiza una consulta de la tabla Articulos
     *
     * @param codigo String con el codigo que se desea consultar
     * @return Articulo contiene los datos del articulo
     * @throws Exception
     */
    public Articulo consultarArticulo(String codigo) throws Exception {

        String sql = "SELECT * FROM Articulos WHERE codigo='" + codigo + "'";
        Articulo resultado = null;

        ResultSet rs = stBusqueda.executeQuery(sql);

        if (rs.next()) {
            resultado = new Articulo(rs.getString("codigo"), rs.getString("descripcion"),
                    rs.getDouble("stock"), rs.getDouble("stock_minimo"), rs.getDouble("precio_compra"), rs.getDouble("precio_venta"));
        }

        return resultado;
    }

    /**
     * Actualiza los datos de un articulo
     *
     * @param articulo Articulo que contiene los datos del articulo
     * @throws Exception
     */
    public void actualizarArticulo(Articulo articulo) throws Exception {
        String sqlModificar = "UPDATE Articulos SET ";

        TreeMap datosModificos = articulo.datosModificas();

        if ((Boolean) datosModificos.get((String) "descripcion")) {
            sqlModificar += "descripcion='" + articulo.getDescripcion() + "'";
        }

        if ((Boolean) datosModificos.get((String) "stock")) {
            sqlModificar += "stock=" + articulo.getStock();
        }

        if ((Boolean) datosModificos.get((String) "stock_minimo")) {
            sqlModificar += "stock_minimo=" + articulo.getStock_minimo();
        }

        if ((Boolean) datosModificos.get((String) "precio_compra")) {
            sqlModificar += "precio_compra=" + articulo.getPrecio_compra();
        }

        if ((Boolean) datosModificos.get((String) "precio_venta")) {
            sqlModificar += "precio_venta=" + articulo.getPrecio_venta();
        }

        sqlModificar += " WHERE codigo='" + articulo.getCodigo() + "'";

        stBusqueda.executeUpdate(sqlModificar);

        articulo.resetearIndicadoresModificados();
    }

    /**
     * Modifica el stock de un articulo
     *
     * @param codigo String con el codigo del articulo
     * @param nuevoStock double con el nuevo valor
     * @throws Exception
     */
    public void actualizarStock(String codigo, double nuevoStock) throws Exception {
        String sqlModificar = "Update Articulos SET stock=" + nuevoStock + " Where codigo='" + codigo + "'";
        stBusqueda.executeUpdate(sqlModificar);
    }

    /**
     * Modifica el total_ventas de un cliente/proveedor
     *
     * @param codigo String con el codigo del cliente/proveedor
     * @param nuevoTotal double con el nuevo valor
     * @param esCliente boolean que indica si es cliente o preveedor
     * @throws Exception
     */
    public void actualizarTotalVentas(String codigo, double nuevoTotal, boolean esCliente) throws Exception {

        String tabla;
        if (esCliente) {
            tabla = "Clientes";
        } else {
            tabla = "Proveedores";
        }

        String sqlModificar = "Update " + tabla + " SET total_ventas=" + nuevoTotal + " Where codigo='" + codigo + "'";
        stBusqueda.executeUpdate(sqlModificar);
    }

    public void insertarEnHistorica(Historica historica) throws Exception {

        String insert = "INSERT INTO Historica VALUES (?,?,?,?,?)";

        PreparedStatement psGuardar;
        psGuardar = con.prepareStatement(insert);

        psGuardar.setString(1, historica.getCliente());
        psGuardar.setString(2, historica.getProveedor());
        psGuardar.setString(3, historica.getArticulo());
        psGuardar.setInt(4, historica.getUnidades());
        psGuardar.setDate(5, historica.getFecha());

        psGuardar.executeUpdate();

        psGuardar.close();
    }

}
