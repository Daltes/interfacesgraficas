CREATE TABLE Clientes (
	codigo varchar(6) NOT NULL,
	nif varchar(9),
	apellidos varchar(35),
	nombre varchar(15),
	domicilio varchar(40),
	codigo_postal varchar(5),
	localidad varchar(20),
	telefono varchar(9),
	movil varchar(9),
	fax varchar(9),
	email varchar(30),
	total_ventas float,

    PRIMARY KEY (codigo)
);

insert into Clientes values(
	"000asd",
	"NIF",
	"asd",
	"asd",
	"asd",
	"asd",
	"asd",
	"asd",
	"asd",
	"asd",
	"asd",
	0
);