/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventasDeOrdenadores;

import java.io.Serializable;

/**
 *
 * @author dany-
 */
public class Cliente implements Serializable{

    // Atributos
    int nombre;
    int localidad;
    int procesador;
    int memoria;
    int monitor;
    int discoDuro;
    boolean grabadora;
    boolean wifi;
    boolean tv;
    boolean backup;
    // Atributos

    // Getters y Setters
    public int getNombre() {
        return nombre;
    }

    public void setNombre(int nombre) {
        this.nombre = nombre;
    }

    public int getLocalidad() {
        return localidad;
    }

    public void setLocalidad(int localidad) {
        this.localidad = localidad;
    }

    public int getProcesador() {
        return procesador;
    }

    public void setProcesador(int procesador) {
        this.procesador = procesador;
    }

    public int getMemoria() {
        return memoria;
    }

    public void setMemoria(int memoria) {
        this.memoria = memoria;
    }

    public int getMonitor() {
        return monitor;
    }

    public void setMonitor(int monitor) {
        this.monitor = monitor;
    }

    public int getDiscoDuro() {
        return discoDuro;
    }

    public void setDiscoDuro(int discoDuro) {
        this.discoDuro = discoDuro;
    }

    public boolean isGrabadora() {
        return grabadora;
    }

    public void setGrabadora(boolean grabadora) {
        this.grabadora = grabadora;
    }

    public boolean isWifi() {
        return wifi;
    }

    public void setWifi(boolean wifi) {
        this.wifi = wifi;
    }

    public boolean isTv() {
        return tv;
    }

    public void setTv(boolean tv) {
        this.tv = tv;
    }

    public boolean isBackup() {
        return backup;
    }

    public void setBackup(boolean backup) {
        this.backup = backup;
    }
    // Fin de Getters y Setters

    // Costructor
    public Cliente(int nombre,int localidad, int procesador, int memoria, int monitor, int discoDuro, boolean grabadora, boolean wifi, boolean tv, boolean backup) {
        this.nombre = nombre;
        this.localidad = localidad;
        this.procesador = procesador;
        this.memoria = memoria;
        this.monitor = monitor;
        this.discoDuro = discoDuro;
        this.grabadora = grabadora;
        this.wifi = wifi;
        this.tv = tv;
        this.backup = backup;
    }
    // Fin Contrusctor

    @Override
    public String toString() {
        return "Cliente{" + "nombre=" + nombre + ", localidad=" + localidad + ", procesador=" + procesador + ", memoria=" + memoria + ", monitor=" + monitor + ", discoDuro=" + discoDuro + ", grabadora=" + grabadora + ", wifi=" + wifi + ", tv=" + tv + ", backup=" + backup + '}';
    }
}
