    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import java.util.ArrayList;

/**
 *
 * @author dany
 */
public class Operaciones {

    /**
     * Suma dos numeros
     *
     * @param primerNumero Primer numero que se va a sumar
     * @param segundoNumero Segundo numero que se va a sumar
     * @return Resultado de la suma
     */
    static String sumar(String primerNumero, String segundoNumero) {
        return String.valueOf(Double.valueOf(primerNumero) + Double.valueOf(segundoNumero));
    }

    /**
     * Resta dos numeros
     *
     * @param primerNumero Primer numero que se va a restar
     * @param segundoNumero Segundo numero que se va a restar
     * @return Resultado de la resta
     */
    static String restar(String primerNumero, String segundoNumero) {
        return String.valueOf(Double.valueOf(primerNumero) - Double.valueOf(segundoNumero));
    }

    /**
     * Multiplica dos numeros
     *
     * @param primerNumero Primer numero que se va a multiplicar
     * @param segundoNumero Segundo numero que se va a multiplicar
     * @return Resultado de la multiplicacion
     */
    static String multiplicar(String primerNumero, String segundoNumero) {
        return String.valueOf(Double.valueOf(primerNumero) * Double.valueOf(segundoNumero));
    }

    /**
     * Divide dos numeros
     *
     * @param primerNumero Primer numero que se va a dividir
     * @param segundoNumero Segundo numero que se va a dividir
     * @return Resultado de la divicion
     */
    static String divir(String primerNumero, String segundoNumero) {
        return String.valueOf(Double.valueOf(primerNumero) / Double.valueOf(segundoNumero));
    }

    /**
     * Realiza todas las operaciones con los numeros y signos de operaciones que
     * se le hayan pasado en los arrayLists
     *
     * @param numeros ArrayList con los numeros que se tiene que operar
     * @param operaciones ArrayList con los signos de las operaciones que se se
     * tiene que realizar
     * @return String con el resultado final de las operaciones
     */
    static String realizarOperacion(ArrayList<String> numeros, ArrayList<Character> operaciones) {
        //Se inicializa la variable que contiene el resultado
        String resultado;

        boolean hayMultiplicacionesYDivisiones;
        /*
         Bucle que realizara las operaciones hasta que no quede ninguna operacion
         pendiente
         */
        do {
            /*
             Boolean para saber si las operaciones pendientes contienen
             multiplicaciones o divisiones
             */
            hayMultiplicacionesYDivisiones = operaciones.contains('*')
                    || operaciones.contains('/');
            /*
             Se recorre el array de operaciones para filtrar entre
             multiplicaciones y divisiones de las sumas y restas, asi para
             realizar las operaciones jerarquicas correspondientes.
             */
            for (int i = 0; i < operaciones.size(); i++) {
                //Se comprueba si hay multiplicaciones y divisones pendientes
                if (hayMultiplicacionesYDivisiones) {
                    /*
                     Si hay multiplicaciones o divisiones pendientes se busca la
                     posicion de la multiplicacion/division para realizar la 
                     operacion
                     */
                    if (operaciones.get(i) == '*'
                            || operaciones.get(i) == '/') {
                        /*
                         Una vez encontrada la multiplicacion o division se
                         realiza la operacion correspondiente
                         */
                        switch (operaciones.get(i)) {
                            case '*':
                                /*
                                 Caso para realizar una multiplicacion
                                 */
                                resultado = multiplicar(numeros.get(i), numeros.get(i + 1));
                                /*
                                 Se eleminan los numeros del arrayLis con los que
                                 se ha hecho la operacion, y tambien se elimina
                                 el signo de la operacion con la que se ha
                                 realizado la operacion
                                 */
                                limpiarArrayList(numeros, i, operaciones, resultado);
                                break;
                            case '/':
                                /*
                                 Caso para realizar una division
                                 */
                                resultado = divir(numeros.get(i), numeros.get(i + 1));
                                /*
                                 Se eleminan los numeros del arrayLis con los que
                                 se ha hecho la operacion, y tambien se elimina
                                 el signo de la operacion con la que se ha
                                 realizado la operacion
                                 */
                                limpiarArrayList(numeros, i, operaciones, resultado);
                                break;
                        }
                    }
                } else {
                    /*
                     Si ya no quedan multiplicaiones y divisiones se realizan las
                     sumas y restas
                     */
                    switch (operaciones.get(i)) {
                        case '+':
                            /*
                             Caso para realizar una suma
                             */
                            resultado = sumar(numeros.get(i), numeros.get(i + 1));
                            /*
                             Se eleminan los numeros del arrayLis con los que
                             se ha hecho la operacion, y tambien se elimina
                             el signo de la operacion con la que se ha
                             realizado la operacion
                             */
                            limpiarArrayList(numeros, i, operaciones, resultado);
                            break;
                        case '-':
                            /*
                             Caso para realizar una resta
                             */
                            resultado = restar(numeros.get(i), numeros.get(i + 1));
                            /*
                             Se eleminan los numeros del arrayLis con los que
                             se ha hecho la operacion, y tambien se elimina
                             el signo de la operacion con la que se ha
                             realizado la operacion
                             */
                            limpiarArrayList(numeros, i, operaciones, resultado);
                            break;
                    }
                }
            }
            //System.out.println(resultado);

            /*
             Cuando no quede ningun signo de operacion en el array significa
             que se han realizado todas las operacions y se puede terminar el
             bucle
             */
        } while (!operaciones.isEmpty());
        /*
         Las operaciones se van realizando y eliminando los numeros de las 
         operaciones que se han realizado, una vez completados todas las
         operacions solo queda un unico numero en el arrayList que es el resultado
         final.
         */
        return numeros.get(
                0);
    }

    /**
     * Elimina de los arrayList los numeros y el signo de la posicion que se
     * indique.
     *
     * @param numeros ArrayList con los numeros que se usan para las operaciones
     * @param i int numero que indica la posicion que se desea eliminar
     * @param operaciones ArrayList con los signos de operaciones a ralizar
     * @param resultado String con el resultado de la operacion, se sustituye
     * por los numeros eliminados
     */
    private static void limpiarArrayList(ArrayList<String> numeros, int i, ArrayList<Character> operaciones, String resultado) {
        // Se elimina el primer numero
        numeros.remove(i);
        // Se elimina el segundo numero
        numeros.remove(i);
        // Se elimina el signo de operacion
        operaciones.remove(i);
        // Se sustituye los numeros eliminados por el resultado
        numeros.add(i, resultado);
    }

}
