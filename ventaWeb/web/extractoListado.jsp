<%-- 
    Document   : extractoListado
    Created on : 27-ene-2019, 18:32:50
    Author     : dany-
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.Date"%>
<%@page import="controladores.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            td {
               padding-right: 15px; 
            }
        </style>
    </head>
    <body>
        <%
            HttpSession sesion = request.getSession(true);
            GestorBBDD bd = (GestorBBDD) sesion.getAttribute("conexion");
            if (bd == null) {
                bd = new GestorBBDD();
                try {
                    bd.conectarBD();
                } catch (Exception e) {
                    %>
                        <meta http-equiv="refresh" content="0; url=errorCon.jsp" />
                    <%
                }
                sesion.setAttribute("conexion", bd);
            }
            
            Cliente cliente = (Cliente) sesion.getAttribute("cliente");
            String codigo = request.getParameter("txtCodigo");
            if(cliente == null || codigo != null){
                try {
                    cliente = bd.realizarConsultaClientes(codigo);
                } catch (Exception e) {
                    %>
                        <meta http-equiv="refresh" content="0; url=errorCon.jsp" />
                    <%
                }
                sesion.setAttribute("cliente", cliente);
            }
            
        %>
        <h1>Gestión de extractos</h1>
        <% 
            if (cliente == null) {
                %>
                    <h2>El cliente con código <%=codigo %> no existe</h2>
                    <a href="extractoCliente.jsp" style="border-right: 1px solid blue;padding-right: 5px;margin-right: 5px"> Nuevo Cliente </a>
                    <a href="index.jsp"> Página principal </a>
                <%
            } else {
                %>
                <h2>Datos del cliente</h2>
                <br/>
                <table>
                    <tr>
                        <td><strong>Código</strong></td>
                        <td><strong>N.I.F.</strong></td>
                        <td><strong>Nombre</strong></td>
                        <td><strong>Apellidos</strong></td>
                    </tr>
                    <tr>
                        <td><%=cliente.getCodigo() %></td>
                        <td><%=cliente.getNif() %></td>
                        <td><%=cliente.getNombre() %></td>
                        <td><%=cliente.getApellido() %></td>
                    </tr>
                    <tr>
                        <td><strong>Domicilio</strong></td>
                        <td><strong>C.P.</strong></td>
                        <td><strong>Localidad</strong></td>
                        <td><strong>Total</strong></td>
                    </tr>
                    <tr>
                        <td><%=cliente.getDomicilio() %></td>
                        <td><%=cliente.getCodigo_postal() %></td>
                        <td><%=cliente.getLocalidad() %></td>
                        <td><%=cliente.getTotal_ventas() %></td>
                    </tr>
                </table>
                <br/>
                <hr/>
                <br/>
                <%
                    SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
                    Carrito carrito = bd.buscarVentas(cliente, new Date(formato.parse(request.getParameter("fechaInicio")).getTime()), new Date(formato.parse(request.getParameter("fechaFin")).getTime()));
                    if(carrito.numeroVentas() == 0){
                        %>
                            <h1>No se ha hecho pedidos entre las fechas</h1>
                            <h1><%= request.getParameter("fechaInicio") %> y <%= request.getParameter("fechaFin") %></h1>
                            <hr/>
                            <br/>
                            <br/>
                            <form action="extractoFecha.jsp">
                                <input type="submit" value="Nuevas fechas"/>
                            </form>
                            <br/>
                            <a href="extractoCliente.jsp" style="border-right: 1px solid blue;padding-right: 5px;margin-right: 5px"> Nuevo Cliente </a>
                            <a href="index.jsp"> Página principal </a>
                        <%
                    } else {
                        %>
                        <h2>Extracto de pedido</h2>
                        <h3>Del <%= request.getParameter("fechaInicio") %> al <%= request.getParameter("fechaFin") %></h3>
                        <table>
                            <tr>
                                <td style="font-weight: bold">Fecha</td>
                                <td style="font-weight: bold">Artículo</td>
                                <td style="font-weight: bold; text-align: right">Unidades</td>
                            </tr>

                            <%

                                sesion.setAttribute("carritoListado", carrito);
                                sesion.setAttribute("fechaInicio", request.getParameter("fechaInicio"));
                                sesion.setAttribute("fechaFin", request.getParameter("fechaFin"));
                                for (int i = 0; i < carrito.numeroVentas(); i++) {
                                    out.println("<tr>");
                                        out.println("<td>");
                                            out.println( formato.format(carrito.ventaWeb(i).getFecha().getTime()));
                                        out.println("</td>");

                                        out.println("<td>");
                                            out.println(carrito.ventaWeb(i).getArticulo().getCodigo());
                                        out.println("</td>");

                                        out.println("<td style=\"text-align: right\">");
                                            out.println(carrito.ventaWeb(i).getUnidades());
                                        out.println("</td>");
                                    out.println("</tr>");
                                }
                            %>
                        </table>
                        <br/>
                        <hr/>
                        <br/>
                        <h2>Extracto finalizado</h2>
                        <p>
                            Si desea imprimir el pedido se abrir&aacute; una nueva ventana con la
                            factura a imprimir.
                            <br/>
                            En esta ventana, abra el men&uacute; Archivo y ejecute la opci&oacute;n
                            Imprimir.
                            <br/>
                            En la nueva ventana, seleccione su impresora y pulse Imprimir.
                            <br/>
                            Despu&eacute;s, cierre la ventana que contiene la factura a imprimir.
                            <br/>
                            Para imprimir el pedido pulse
                            <a href="extractoImprimir.jsp" target="_blank">aqu&iacute;.</a>
                            Si no va a imprimir, puede regresar a la
                            <a href="index.jsp">p&aacute;gina principal.</a>
                        </p>
                    <%
                }
            }
        %>
    </body>
</html>
