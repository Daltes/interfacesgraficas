<%-- 
    Document   : gestionPedido
    Created on : 24-ene-2019, 11:18:54
    Author     : alumno
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="controladores.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cliente</title>
    </head>
    <body>
        <%
            HttpSession sesion = request.getSession(true);
            GestorBBDD bd = (GestorBBDD) sesion.getAttribute("conexion");
            if (bd == null) {
                bd = new GestorBBDD();
                try {
                    bd.conectarBD();
                } catch (Exception e) {
                    %>
                        <meta http-equiv="refresh" content="0; url=errorCon.jsp" />
                    <%
                }
                sesion.setAttribute("conexion", bd);
            }
        %>
        <h1>Gestión de pedidos</h1>
        <form action="articulo.jsp" name="formulario" onsubmit="return Correcto_submit()" method="POST">
            Código cliente: <input type="text" autofocus name="txtCodigo">
            <br>
            <br>
            <input type="submit" value="Aceptar">
            <input type="reset" value="Cancelar" onclick="document.formulario.txtCodigo.focus()">
        </form>
        <br>
        <a href="index.jsp"> Página principal </a>

        <script type="text/javascript">
            function Correcto_submit() {
                with (document.formulario.txtCodigo) {
                    if (value === "") {
                        alert("No ha introducido ningún código");
                        focus();
                        return false;
                    } else if(value.match(/^[a-z0-9]{1,6}$/i)){
                        var textoFinal = "";
                        var codigo = value.toString();
                        for(var i = 0; i < 6 - codigo.length; i++){
                            textoFinal += "0";
                        }
                        value = textoFinal + codigo;
                        return true;
                    } else {
                        alert("El código introducido no es válido. \nEl código tiene que tener solo letras y números, y una longitud máxima de 6.");
                        focus();
                        return false;
                    }
                        
                }
            }
        </script>

    </body>
</html>
