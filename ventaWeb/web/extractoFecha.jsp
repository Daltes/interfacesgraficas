<%-- 
    Document   : extractoFecha
    Created on : 27-ene-2019, 17:40:35
    Author     : dany-
--%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="controladores.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    <body>
        <%
            HttpSession sesion = request.getSession(true);
            GestorBBDD bd = (GestorBBDD) sesion.getAttribute("conexion");
            if (bd == null) {
                bd = new GestorBBDD();
                try {
                    bd.conectarBD();
                } catch (Exception e) {
                    %>
                        <meta http-equiv="refresh" content="0; url=errorCon.jsp" />
                    <%
                }
                sesion.setAttribute("conexion", bd);
            }
            
            Cliente cliente = (Cliente) sesion.getAttribute("cliente");
            String codigo = request.getParameter("txtCodigo");
            if(cliente == null || codigo != null){
                try {
                    cliente = bd.realizarConsultaClientes(codigo);
                } catch (Exception e) {
                    %>
                        <meta http-equiv="refresh" content="0; url=errorCon.jsp" />
                    <%
                }
                sesion.setAttribute("cliente", cliente);
            }
            
        %>
        <h1>Gestión de extractos</h1>
        <% 
            if (cliente == null) {
                %>
                    <h2>El cliente con código <%=codigo %> no existe</h2>
                <%
            } else {
                %>
                    <h2>Datos del cliente</h2>
                    <br/>
                    <table>
                        <tr>
                            <td><strong>Código</strong></td>
                            <td><strong>N.I.F.</strong></td>
                            <td><strong>Nombre</strong></td>
                            <td><strong>Apellidos</strong></td>
                        </tr>
                        <tr>
                            <td><%=cliente.getCodigo() %></td>
                            <td><%=cliente.getNif() %></td>
                            <td><%=cliente.getNombre() %></td>
                            <td><%=cliente.getApellido() %></td>
                        </tr>
                        <tr>
                            <td><strong>Domicilio</strong></td>
                            <td><strong>C.P.</strong></td>
                            <td><strong>Localidad</strong></td>
                            <td><strong>Total</strong></td>
                        </tr>
                        <tr>
                            <td><%=cliente.getDomicilio() %></td>
                            <td><%=cliente.getCodigo_postal() %></td>
                            <td><%=cliente.getLocalidad() %></td>
                            <td><%=cliente.getTotal_ventas() %></td>
                        </tr>
                    </table>
                    <br/>
                    <hr/>
                    <br/>
                    
                    <h2>Fechas del extracto</h2>
                    <form action="extractoListado.jsp" name="formulario" onsubmit="return Correcto_submit()">
                        <table>
                            <tr>
                                <td colspan="4">Desde</td>
                                <td colspan="3">Hasta</td>
                            </tr>
                            <tr>
                                <td><strong> Día </strong></td>
                                <td><strong> Mes </strong></td>
                                <td><strong> Año </strong></td>
                                <td style="padding-right:20px"></td>
                                <td><strong> Día </strong></td>
                                <td><strong> Mes </strong></td>
                                <td><strong> Año </strong></td>
                            </tr>
                            <tr>
                                <%
                                    SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
                                    String fecha = formato.format(new Date(System.currentTimeMillis()));
                                %>
                                <td colspan="3"><input type="date" name="fechaInicio" value="<%= fecha %>" max="<%= fecha %>"></td>
                                <td style="padding-right:20px"></td>
                                <td colspan="3"><strong> <input type="date" name="fechaFin" value="<%= fecha %>" max="<%= fecha %>"> </strong></td>
                            </tr>
                        </table>
                        <br/>
                        <input type="submit" value="Aceptar"/>
                    </form>
                <%   
            }
        %>
        <br/>
        <hr/>
        <br/>
        <a href="extractoCliente.jsp" style="border-right: 1px solid blue;padding-right: 5px;margin-right: 5px"> Nuevo Cliente </a>
        <a href="index.jsp"> Página principal </a>
        <script type="text/javascript">
            function Correcto_submit() {
                if(document.formulario.fechaInicio.value > document.formulario.fechaFin.value){
                    alert("Fechas incorrectas\nLa fecha 'Desde' es mayor a la fecha 'Hasta'");
                    return false;
                }
                    return true;
            }
        </script>
    </body>
</html>
