<%-- 
    Document   : pedidoFinalizado
    Created on : 26-ene-2019, 17:10:19
    Author     : dany-
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="controladores.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pedido Finalizado</title>
        <style>
            .centrado{
                text-align: center;
                margin-left: auto;
                margin-right: auto;
            }
            td {
                padding-right: 10px;
            }
            .numeroDinero {
                text-align: right;
            }
            .centrarTabla{
                margin-left: auto;
                margin-right: auto;
                font-size: 20px;
            }
            .tituloTabla{
                font-size: 25px;
            }
            .filaVaciaTabla{
                height: 20px;
            }
            .izquierda{
                text-align: right;
            }
        </style>
    </head>
    <body>
        <%
            HttpSession sesion = request.getSession(true);
            GestorBBDD bd = (GestorBBDD) sesion.getAttribute("conexion");
            if (bd == null) {
                bd = new GestorBBDD();
                try {
                    bd.conectarBD();
                } catch (Exception e) {
                    %>
                        <meta http-equiv="refresh" content="0; url=errorCon.jsp" />
                    <%
                }
                sesion.setAttribute("conexion", bd);
            }
            Carrito carrito = (Carrito) sesion.getAttribute("carrito");
            
            Boolean esNuevo = (Boolean) sesion.getAttribute("esNuevo");
            if(esNuevo == null || !esNuevo){
                esNuevo = true;
                sesion.setAttribute("esNuevo", esNuevo);
            }
        %>
        <h1>Pedido finalizado</h1>
        <%
            if(carrito == null || carrito.numeroVentas() == 0){
                %>
                    <h1>No se ha realizado ninguna compra</h1>
                <%
            } else {
                %>
                    <p>
                        Si desea imprimir el pedido se abrir&aacute; una nueva ventana con la
                        factura a imprimir.
                        <br/>
                        En esta ventana, abra el men&uacute; Archivo y ejecute la opci&oacute;n
                        Imprimir.
                        <br/>
                        En la nueva ventana, seleccione su impresora y pulse Imprimir.
                        <br/>
                        Despu&eacute;s, cierre la ventana que contiene la factura a imprimir.
                        <br/>
                        Para imprimir el pedido pulse
                        <a href="imprimir.jsp" target="_blank">aqu&iacute;.</a>
                        Si no va a imprimir, puede regresar a la
                        <a href="index.jsp">p&aacute;gina principal.</a></p>
                    <br/>


                    <hr/>
                    <%
                        java.sql.Date fechaSistema = new java.sql.Date((new java.util.Date()).getTime());
                        String fecha = String.valueOf(fechaSistema);
                        String fecha1 = fecha.substring(8, 10);
                        fecha1 = fecha1 + fecha.substring(4, 7);
                        fecha1 = fecha1 + "-" + fecha.substring(0, 4);
                    %>
                    <p>Fecha : <%=fecha1 %></p>
                    <p>FACTURA N.: 1 </p>
                    <hr/>
                    <h1 class="centrado">Empresa Proyecto Web de clase, S.A.</h1>
                    <h2 class="centrado">N.I.F. 12345678-A</h2>
                    <h3 class="centrado">C/ Isla de Sálvora, 451 - 28400 Collado Villalba - Madrid</h3>
                    <hr/>
                    <p>
                        <strong>Cliente: </strong><%= carrito.ventaWeb(0).getCliente().getCodigo()%> <strong>N.I.F.:</strong> <%= carrito.ventaWeb(0).getCliente().getNif() %>
                        <br/>
                        <strong>D./Dña. </strong><%= carrito.ventaWeb(0).getCliente().getNombre()%> <%= carrito.ventaWeb(0).getCliente().getApellido()%>
                        <br/>
                        <%= carrito.ventaWeb(0).getCliente().getDomicilio()%>
                        <br/>
                        <%= carrito.ventaWeb(0).getCliente().getCodigo_postal()%> <%= carrito.ventaWeb(0).getCliente().getLocalidad()%>
                    </p>
                    <hr/>
                    <div>
                        <table class="centrarTabla">
                            
                            <tr>
                                <td class="tituloTabla"><strong>Artículo</strong></td>
                                <td class="tituloTabla"><strong>Descripción</strong></td>
                                <td class="tituloTabla"><strong>Unidades</strong></td>
                                <td class="tituloTabla"><strong>Precio</strong></td>
                                <td class="tituloTabla"><strong>Importe</strong></td>
                            </tr>
                            <tr class="filaVaciaTabla"></tr>
                            <%
                                
                                for (int i = 0; i < carrito.numeroVentas(); i++) {
                                        out.println("<tr><td>" + carrito.ventaWeb(i).getArticulo().getCodigo() + "</td>");
                                        out.println("<td>" + carrito.ventaWeb(i).getArticulo().getDescripcion() + "</td>");
                                        out.println("<td class=\"numeroDinero\">" + carrito.precio(i) + "</td>");  
                                        out.println("<td class=\"numeroDinero\">" + carrito.ventaWeb(i).getUnidades() + "</td>");                                   
                                        out.println("<td class=\"numeroDinero\">" + carrito.costeTotal(i) + "</td></tr>");
                                }
                            %>
                        </table>
                    </div>
                    <hr/>
                    <h1 class="izquierda">Total Factura (I.V.A. inc.): <%= carrito.precioTotal() %></h1>
                    <hr/>
                <%
            }
        %>
        <script>
            function Correcto_submit() {
                with (document.formulario.unidades) {
                    <%
                        try{
                            bd.guardarCarrito(carrito);
                            %>
                                return true;
                            <%
                        }catch(Exception e){
                            %>
                                <meta http-equiv="refresh" content="0; url=errorCon.jsp" />
                                return false;
                            <%
                        }
                    %>
                }
            }
        </script>
    </body>
</html>
