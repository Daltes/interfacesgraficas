<%-- 
    Document   : realizarPedidos
    Created on : 24-ene-2019, 13:03:17
    Author     : alumno
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="controladores.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Artículo</title>
        <style>
            td {
                padding-right: 10px;
            }
        </style>
    </head>
    <body>
        <%
            HttpSession sesion = request.getSession(true);
            GestorBBDD bd = (GestorBBDD) sesion.getAttribute("conexion");
            if (bd == null) {
                bd = new GestorBBDD();
                try {
                    bd.conectarBD();
                } catch (Exception e) {
                    %>
                        <meta http-equiv="refresh" content="0; url=errorCon.jsp" />
                    <%
                }
                sesion.setAttribute("conexion", bd);
            }
            
            Cliente cliente = (Cliente) sesion.getAttribute("cliente");
            String codigo = request.getParameter("txtCodigo");
            if(cliente == null || codigo != null){
                try {
                    cliente = bd.realizarConsultaClientes(codigo);
                } catch (Exception e) {
                    %>
                        <meta http-equiv="refresh" content="0; url=errorCon.jsp" />
                    <%
                }
                sesion.setAttribute("cliente", cliente);
            }
            
        %>
        <h1>Gestión de pedidos</h1>
        <% 
            if (cliente == null) {
                %>
                    <h2>El cliente con código <%=codigo %> no existe</h2>
                <%
            } else {
                %>
                    <h2>Datos del cliente</h2>
                    <br/>
                    <table>
                        <tr>
                            <td><strong>Código</strong></td>
                            <td><strong>N.I.F.</strong></td>
                            <td><strong>Nombre</strong></td>
                            <td><strong>Apellidos</strong></td>
                        </tr>
                        <tr>
                            <td><%=cliente.getCodigo() %></td>
                            <td><%=cliente.getNif() %></td>
                            <td><%=cliente.getNombre() %></td>
                            <td><%=cliente.getApellido() %></td>
                        </tr>
                        <tr>
                            <td><strong>Domicilio</strong></td>
                            <td><strong>C.P.</strong></td>
                            <td><strong>Localidad</strong></td>
                            <td><strong>Total</strong></td>
                        </tr>
                        <tr>
                            <td><%=cliente.getDomicilio() %></td>
                            <td><%=cliente.getCodigo_postal() %></td>
                            <td><%=cliente.getLocalidad() %></td>
                            <td><%=cliente.getTotal_ventas() %></td>
                        </tr>
                    </table>
                    <br/>
                    <hr/>
                    <br/>
                    <h2>Realizar pedido</h2>
                    <form action="pedido.jsp" name="formulario" onsubmit="return Correcto_submit()" method="POST">
                        <p>Artículo</p>
                        <input type="text" autofocus name="txtCodigo"/>
                        <br/>
                        <br/>
                        <input type="submit" value="Aceptar"/>
                        <input type="reset" value="Cancelar" onclick="document.formulario.txtCodigo.focus()"/>
                    </form>
                    <br/>
                    <hr/>
                    <br/>
                <%
            }
        %>
                    
        <a href="cliente.jsp" style="border-right: 1px solid blue;padding-right: 5px;margin-right: 5px"> Nuevo Cliente </a>
        <a href="index.jsp"> Página principal </a>
        
        <script type="text/javascript">
            function Correcto_submit() {
                with (document.formulario.txtCodigo) {
                    if (value === "") {
                        alert("No ha introducido ningún código");
                        focus();
                        return false;
                    } else if(value.match(/^[a-z0-9]{1,6}$/i)){
                        var textoFinal = "";
                        var codigo = value.toString();
                        for(var i = 0; i < 6 - codigo.length; i++){
                            textoFinal += "0";
                        }
                        value = textoFinal + codigo;
                        return true;
                    } else {
                        alert("El código introducido no es válido. \nEl código tiene que tener solo letras y números, y una longitud máxima de 6.");
                        focus();
                        return false;
                    }
                }
            }
        </script>
    </body>
</html>
