<%-- 
    Document   : pedido
    Created on : 26-ene-2019, 13:42:01
    Author     : dany-
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="controladores.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pedido</title>
        <style>
            td {
                padding-right: 10px;
            }
            .numeroDinero {
                text-align: right;
            }
        </style>
    </head>
    <body>
        <%
            HttpSession sesion = request.getSession(true);
            GestorBBDD bd = (GestorBBDD) sesion.getAttribute("conexion");
            if (bd == null) {
                bd = new GestorBBDD();
                try {
                    bd.conectarBD();
                } catch (Exception e) {
                    %>
                        <meta http-equiv="refresh" content="0; url=errorCon.jsp" />
                    <%
                }
                sesion.setAttribute("conexion", bd);
            }
            
            Cliente cliente = (Cliente) sesion.getAttribute("cliente");
            
            Carrito carrito = (Carrito) sesion.getAttribute("carrito");
            if(carrito == null){
                carrito = new Carrito();
                sesion.setAttribute("carrito", carrito);
            }
            
            String unidades = request.getParameter("unidades");
            String importe = request.getParameter("importe");
            
            Articulo articulo = (Articulo) sesion.getAttribute("articulo");
            
            if(carrito.numeroVentas() != 0 && !carrito.ventaWeb(0).getCliente().getCodigo().equals(cliente.getCodigo())){
                carrito.vaciarCarrito();
            }
            
            if(!(unidades == null || importe == null)){
                carrito.guardarVentaWeb(new VentaWeb(cliente, articulo, Integer.parseInt(unidades)));
            } 
            
        %>
        <h1>Gestión de pedidos</h1>
        <% 
            if (cliente == null || articulo == null) {
                if(cliente == null){
                    %> 
                        <h2>No se ha podido encontrar el cliente</h2>
                    <%
                } else if (articulo == null) {
                    %> 
                        <h2>No se ha podido encontrar el artículo</h2>
                    <%
                }
            } else {
                %> 
                    <!-- Datos del cliente -->
                    <h2>Datos del cliente</h2>
                    <br/>
                    <table>
                        <tr>
                            <td><strong>Código</strong></td>
                            <td><strong>N.I.F.</strong></td>
                            <td><strong>Nombre</strong></td>
                            <td><strong>Apellidos</strong></td>
                        </tr>
                        <tr>
                            <td><%=cliente.getCodigo() %></td>
                            <td><%=cliente.getNif() %></td>
                            <td><%=cliente.getNombre() %></td>
                            <td><%=cliente.getApellido() %></td>
                        </tr>
                        <tr>
                            <td><strong>Domicilio</strong></td>
                            <td><strong>C.P.</strong></td>
                            <td><strong>Localidad</strong></td>
                            <td><strong>Total</strong></td>
                        </tr>
                        <tr>
                            <td><%=cliente.getDomicilio() %></td>
                            <td><%=cliente.getCodigo_postal() %></td>
                            <td><%=cliente.getLocalidad() %></td>
                            <td><%=cliente.getTotal_ventas() %></td>
                        </tr>
                    </table>
                    <br/>
                    <hr/>
                    <br/>
                    <!-- FIN datos del cliente -->
                    
                    <!-- Pedido articulo -->
                    <h2>Realizar pedido</h2>
                    <form action="pedidoFinalizado.jsp" name="formulario" onsubmit="return Correcto_submit()" method="POST">
                        
                        <table>
                            
                            <tr>
                                <td><strong>Artículo</strong></td>
                                <td><strong>Descripción</strong></td>
                                <td><strong>Unidades</strong></td>
                                <td><strong>Precio</strong></td>
                                <td><strong>Importe</strong></td>
                            </tr>
                            <%
                                
                                for (int i = 0; i < carrito.numeroVentas(); i++) {
                                        out.println("<tr><td>" + carrito.ventaWeb(i).getArticulo().getCodigo() + "</td>");
                                        out.println("<td>" + carrito.ventaWeb(i).getArticulo().getDescripcion() + "</td>");
                                        out.println("<td class=\"numeroDinero\">" + carrito.ventaWeb(i).getUnidades() + "</td>");
                                        out.println("<td class=\"numeroDinero\">" + carrito.precio(i) + "</td>");                                        
                                        out.println("<td class=\"numeroDinero\">" + carrito.costeTotal(i) + "</td></tr>");
                                }
                            %>
                        </table>
                        
                        <h2>Importe del pedido: <%= carrito.precioTotal() %></h2>
                        <input type="submit" value="Aceptar pedido"/>
                    </form>
                    <br/>
                    <hr/>
                    <br/>
                    <!-- FIN pedido articulo -->
                <%
            }
        %>
        <a href="articulo.jsp" style="border-right: 1px solid blue;padding-right: 5px;margin-right: 5px"> Pedir otro artículo </a>
        <a href="index.jsp"> Página principal </a>
    </body>
</html>
