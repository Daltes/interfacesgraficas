<%-- 
    Document   : index
    Created on : 22-ene-2019, 8:41:59
    Author     : alumno
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="controladores.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Proyecto Web</title>
    </head>
    <body>
        <%
            HttpSession sesion = request.getSession(true);
            GestorBBDD bd = (GestorBBDD) sesion.getAttribute("conexion");
            if (bd == null) {
                bd = new GestorBBDD();
                try {
                    bd.conectarBD();
                    sesion.setAttribute("conexion", bd);
                } catch (Exception e) {
                    %>
                        <meta http-equiv="refresh" content="0; url=errorCon.jsp" />
                    <%
                }
            }
            Boolean esNuevo = (Boolean) sesion.getAttribute("esNuevo");
            if(esNuevo == null){
                esNuevo = false;
                sesion.setAttribute("esNuevo", esNuevo);
            }

                Carrito carrito = (Carrito) sesion.getAttribute("carrito");
                if(carrito != null){
                    carrito.vaciarCarrito();
                }
        %>
        <h1>Proyecto Web con Base de Datos</h1>
        <a href="cliente.jsp"> Gestión de pedidos </a>
        <br/>
        <a href="extractoCliente.jsp"> Gestión de extractos </a>
    </body>
</html>
