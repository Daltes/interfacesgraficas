<%-- 
    Document   : extractoCliente
    Created on : 27-ene-2019, 17:37:50
    Author     : dany-
--%>

<%@page import="controladores.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Extracti cliente</title>
    </head>
    <body>
         <%
            HttpSession sesion = request.getSession(true);
            GestorBBDD bd = (GestorBBDD) sesion.getAttribute("conexion");
            if (bd == null) {
                bd = new GestorBBDD();
                try {
                    bd.conectarBD();
                    sesion.setAttribute("conexion", bd);
                } catch (Exception e) {
                    %>
                        <meta http-equiv="refresh" content="0; url=errorCon.jsp" />
                    <%
                }
            }
        %>
        <h1>Gestión de extractos</h1>
        <form action="extractoFecha.jsp" name="formulario" onsubmit="return Correcto_submit()" method="POST">
            Código cliente: <input type="text" autofocus name="txtCodigo">
            <br>
            <br>
            <input type="submit" value="Aceptar">
            <input type="reset" value="Cancelar" onclick="document.formulario.txtCodigo.focus()">
        </form>
        <br>
        <a href="index.jsp"> Página principal </a>

        <script type="text/javascript">
            function Correcto_submit() {
                with (document.formulario.txtCodigo) {
                    if (value === "") {
                        alert("No ha introducido ningún código");
                        focus();
                        return false;
                    } else if(value.match(/^[a-z0-9]{1,6}$/i)){
                        var textoFinal = "";
                        var codigo = value.toString();
                        for(var i = 0; i < 6 - codigo.length; i++){
                            textoFinal += "0";
                        }
                        value = textoFinal + codigo;
                        return true;
                    } else {
                        alert("El código introducido no es válido. \nEl código tiene que tener solo letras y números, y una longitud máxima de 6.");
                        focus();
                        return false;
                    }
                        
                }
            }
        </script>
    </body>
</html>
