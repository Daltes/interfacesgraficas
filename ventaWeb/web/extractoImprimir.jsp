<%-- 
    Document   : extractoImprimir
    Created on : 27-ene-2019, 19:22:41
    Author     : dany-
--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="controladores.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Imprimir</title>
        <style>
            .centrado{
                text-align: center;
                margin-left: auto;
                margin-right: auto;
            }
            td {
                padding-right: 10px;
            }
            .numeroDinero {
                text-align: right;
            }
            .centrarTabla{
                margin-left: auto;
                margin-right: auto;
                font-size: 20px;
            }
            .tituloTabla{
                font-size: 25px;
            }
            .filaVaciaTabla{
                height: 20px;
            }
            .izquierda{
                text-align: right;
            }
        </style>
    </head>
    <body>
        <%
            HttpSession sesion = request.getSession(true);
            GestorBBDD bd = (GestorBBDD) sesion.getAttribute("conexion");
            if (bd == null) {
                bd = new GestorBBDD();
                try {
                    bd.conectarBD();
                } catch (Exception e) {
                    %>
                        <meta http-equiv="refresh" content="0; url=errorCon.jsp" />
                    <%
                }
                sesion.setAttribute("conexion", bd);
            }
            Carrito carrito = (Carrito) sesion.getAttribute("carritoListado");
            
            java.sql.Date fechaSistema = new java.sql.Date((new java.util.Date()).getTime());
            String fecha = String.valueOf(fechaSistema);
            String fecha1 = fecha.substring(8, 10);
            fecha1 = fecha1 + fecha.substring(4, 7);
            fecha1 = fecha1 + "-" + fecha.substring(0, 4);
        %>
        <p>Fecha : <%=fecha1 %></p>
        <hr/>
        <h1 class="centrado">Empresa Proyecto Web de clase, S.A.</h1>
        <h2 class="centrado">N.I.F. 12345678-A</h2>
        <h3 class="centrado">C/ Isla de Sálvora, 451 - 28400 Collado Villalba - Madrid</h3>
        <hr/>
        
        <p>
            <strong>Cliente: </strong><%= carrito.ventaWeb(0).getCliente().getCodigo()%> <strong>N.I.F.:</strong> <%= carrito.ventaWeb(0).getCliente().getNif() %>
            <br/>
            <strong>D./Dña. </strong><%= carrito.ventaWeb(0).getCliente().getNombre()%> <%= carrito.ventaWeb(0).getCliente().getApellido()%>
            <br/>
            <%= carrito.ventaWeb(0).getCliente().getDomicilio()%>
            <br/>
            <%= carrito.ventaWeb(0).getCliente().getCodigo_postal()%> <%= carrito.ventaWeb(0).getCliente().getLocalidad()%>
        </p>
        <hr/>
        <h1 class="centrado">Extracto de pedidos desde <%= sesion.getAttribute("fechaInicio") %> hasta <%= sesion.getAttribute("fechaFin") %> </h1>
        <br/>
        <br/>
        <table class="centrarTabla">
            <tr>
                <td style="font-weight: bold">Fecha</td>
                <td style="font-weight: bold">Artículo</td>
                <td style="font-weight: bold; text-align: right">Unidades</td>
            </tr>

            <%
                SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");

                for (int i = 0; i < carrito.numeroVentas(); i++) {
                    out.println("<tr>");
                        out.println("<td>");
                            out.println( formato.format(carrito.ventaWeb(i).getFecha().getTime()));
                        out.println("</td>");

                        out.println("<td>");
                            out.println(carrito.ventaWeb(i).getArticulo().getCodigo());
                        out.println("</td>");

                        out.println("<td style=\"text-align: right\">");
                            out.println(carrito.ventaWeb(i).getUnidades());
                        out.println("</td>");
                    out.println("</tr>");
                }
            %>
        </table>
        <br/>
        <hr/>
    </body>
</html>
