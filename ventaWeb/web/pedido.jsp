<%-- 
    Document   : pedido
    Created on : 26-ene-2019, 13:42:01
    Author     : dany-
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="controladores.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pedido</title>
        <style>
            td {
                padding-right: 10px;
            }
        </style>
    </head>
    <body>
        <%
            HttpSession sesion = request.getSession(true);
            GestorBBDD bd = (GestorBBDD) sesion.getAttribute("conexion");
            if (bd == null) {
                bd = new GestorBBDD();
                try {
                    bd.conectarBD();
                } catch (Exception e) {
                    %>
                        <meta http-equiv="refresh" content="0; url=errorCon.jsp" />
                    <%
                }
                sesion.setAttribute("conexion", bd);
            }
            
            Cliente cliente = (Cliente) sesion.getAttribute("cliente");
            
            Articulo articulo = (Articulo) sesion.getAttribute("articulo");
            String codigo = request.getParameter("txtCodigo");
            if(articulo == null || codigo != null){
                try {
                    articulo = bd.consultarArticulo(codigo);
                } catch (Exception e) {
                    %>
                        <meta http-equiv="refresh" content="0; url=errorCon.jsp" />
                    <%
                }
                sesion.setAttribute("articulo", articulo);
            }
        %>
        <h1>Gestión de pedidos</h1>
        <% 
            if (cliente == null || articulo == null) {
                if(cliente == null){
                    %> 
                        <h2>No se ha podido encontrar el cliente</h2>
                    <%
                } else if (articulo == null) {
                    articulo = new Articulo();
                    %> 
                        <h2>El artículo con código <%=codigo %> no existe</h2>
                    <%
                }
            } else {
                %> 
                    <!-- Datos del cliente -->
                    <h2>Datos del cliente</h2>
                    <br/>
                    <table>
                        <tr>
                            <td><strong>Código</strong></td>
                            <td><strong>N.I.F.</strong></td>
                            <td><strong>Nombre</strong></td>
                            <td><strong>Apellidos</strong></td>
                        </tr>
                        <tr>
                            <td><%=cliente.getCodigo() %></td>
                            <td><%=cliente.getNif() %></td>
                            <td><%=cliente.getNombre() %></td>
                            <td><%=cliente.getApellido() %></td>
                        </tr>
                        <tr>
                            <td><strong>Domicilio</strong></td>
                            <td><strong>C.P.</strong></td>
                            <td><strong>Localidad</strong></td>
                            <td><strong>Total</strong></td>
                        </tr>
                        <tr>
                            <td><%=cliente.getDomicilio() %></td>
                            <td><%=cliente.getCodigo_postal() %></td>
                            <td><%=cliente.getLocalidad() %></td>
                            <td><%=cliente.getTotal_ventas() %></td>
                        </tr>
                    </table>
                    <br/>
                    <hr/>
                    <br/>
                    <!-- FIN datos del cliente -->
                    
                    <!-- Pedido articulo -->
                    <h2>Realizar pedido</h2>
                    <form action="carroCompra.jsp" name="formulario" onsubmit="return Correcto_submit()" method="POST">
                        
                        <table>
                            
                            <tr>
                                <td><strong>Artículo</strong></td>
                                <td><strong>Descripción</strong></td>
                                <td><strong>Unidades</strong></td>
                                <td><strong>Precio</strong></td>
                                <td><strong>Importe</strong></td>
                            </tr>
                            
                            <tr>
                                <td><%=articulo.getCodigo() %></td>
                                <td><%=articulo.getDescripcion() %></td>
                                <td><input type="text" id="unidades" onkeyup="calcularImporte()" onkeypress="validarNumero(event)" autofocus name="unidades"/></td>
                                <td><%=articulo.getPrecio_venta() %></td>
                                <td><input type="text" id="importe" readonly name="importe"/></td>
                            </tr>
                            
                        </table>
                        
                        <br/>
                        <br/>
                        <input type="submit" value="Aceptar"/>
                        <input type="reset" value="Cancelar" onclick="document.formulario.unidades.focus()"/>
                        
                    </form>
                    <br/>
                    <hr/>
                    <br/>
                    <!-- FIN pedido articulo -->
                <%
            }
        %>
                    
        <a href="articulo.jsp" style="border-right: 1px solid blue;padding-right: 5px;margin-right: 5px"> Nuevo artículo </a>
        <a href="index.jsp"> Página principal </a>
        
        <script>
            function validarNumero(event) {
                var caracter = event.which || event.keyCode;
                var unidades = document.getElementById('unidades').value.toString();
                if(!((caracter >= 48 && caracter <= 57 && unidades.length <= 6) || caracter === 8 || caracter === 13 || caracter === 9 || caracter === 116)){
                    event.preventDefault();
                    if(unidades.length < 6){
                        alert("Introduzca un número correcto");
                    }
                        
                }
                
            }
            function calcularImporte() {
                var numeroFinal = document.getElementById('unidades').value * <%=articulo.getPrecio_venta() %>;
                document.getElementById('importe').value= numeroFinal.toFixed(2) ; 
            }
            function Correcto_submit() {
                with (document.formulario.unidades) {
                    if (value === "") {
                        alert("No ha introducido ninguna unidad");
                        focus();
                        return false;
                    } else if(value.match(/^[0-9]+$/i)){
                        if(value < 1){
                            alert("Las unidades introducidas son incorrectas.\nIntroduzca un número mayor a 0.");
                        focus();
                            return false;
                        }    
                        var numeroFinal = document.getElementById('unidades').value * <%=articulo.getPrecio_venta() %>;
                        document.getElementById('importe').value= numeroFinal.toPrecision(2) ; 
                        return true;
                    } else {
                        alert("Las unidades introducidas son incorrectas.\nIntroduzca solo números.");
                        focus();
                        return false;
                    }
                        
                }
            }
        </script>
        
    </body>
</html>
