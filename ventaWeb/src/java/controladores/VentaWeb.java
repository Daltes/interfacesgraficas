/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.sql.Date;

/**
 *
 * @author dany-
 */
public class VentaWeb {
    private Cliente cliente;
    private Articulo articulo;
    private float unidades;
    private Date fecha;

    public float getUnidades() {
        return unidades;
    }

    public void setUnidades(float unidades) {
        this.unidades = unidades;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public VentaWeb(Cliente cliente, Articulo articulo, float unidades) {
        this.cliente = cliente;
        this.articulo = articulo;
        this.unidades = unidades;
        this.fecha = new Date(System.currentTimeMillis());
    }

    public VentaWeb(Cliente cliente, Articulo articulo, float unidades, Date fecha) {
        this.cliente = cliente;
        this.articulo = articulo;
        this.unidades = unidades;
        this.fecha = fecha;
    }
    
    
    
}
