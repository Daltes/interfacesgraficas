/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author dany-
 */
public class GestorBBDD {

    private Connection con;
    private Statement stBusqueda;
    private boolean conectado;

    public boolean isConectado() {
        return conectado;
    }

    public void setConectado(boolean conectado) {
        this.conectado = conectado;
    }

    public GestorBBDD() {
        super();
        conectado = false;
    }

    /**
     * Establece una conexion con la bbdd
     *
     * @throws Exception
     */
    public void conectarBD() throws Exception {

        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

        con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/almacen?serverTimezone=UTC", "root", "root");

        stBusqueda = con.createStatement();
        conectado = true;
    }

    /**
     * Activa y desactiva el autocommit
     *
     * @param estaActivado boolean que indica si se activa o desactiva
     * @throws Exception
     */
    public void estadoAutoCommit(boolean estaActivado) throws Exception {
        con.setAutoCommit(estaActivado);
    }

    /**
     * Realiza un commit
     *
     * @throws java.lang.Exception
     */
    public void hacerCommit() throws Exception {
        con.commit();
    }

    /**
     * Realiza un rollback
     *
     * @throws java.lang.Exception
     */
    public void hacerRollback() throws Exception {
        con.rollback();
    }

    /**
     * Cierra la conexion con la bbdd
     *
     * @throws Exception
     */
    void desconectarBD() throws Exception {
        stBusqueda.close();
        con.close();
    }

    public Cliente realizarConsultaClientes(String codigo) throws Exception {
        return realizarConsultaClientes(codigo, true);
    }

    /**
     * Realiza una busqueda por el codigo
     *
     * @param codigo String que contiene el codigo que se desea buscar
     * @param esCliente Boolean que indica si es un cliente o proveedor
     * @return String con los datos que ha encontrado, null si no encuentra nada
     * @throws java.lang.Exception Se lanzan las excepciones para controlarlas
     * desde la interfaz
     */
    public Cliente realizarConsultaClientes(String codigo, boolean esCliente) throws Exception {
        String nombreTabla;

        if (esCliente) {
            nombreTabla = "Clientes";
        } else {
            nombreTabla = "Proveedores";
        }
        String sqlBusqueda = "SELECT * FROM " + nombreTabla + " WHERE codigo = '" + codigo + "'";

        ResultSet rs;
        rs = stBusqueda.executeQuery(sqlBusqueda);
        Cliente cliente = null;
        while (rs.next()) {

            cliente = new Cliente(rs.getString("codigo"), rs.getString("nif"),
                    rs.getString("apellidos"), rs.getString("nombre"), rs.getString("domicilio"), rs.getString("codigo_postal"),
                    rs.getString("localidad"), rs.getString("telefono"),
                    rs.getString("movil"), rs.getString("fax"), rs.getString("email"), rs.getDouble("total_ventas"));
        }
        rs.close();
        return cliente;
    }

    /**
     * Guarda en la tabla los datos que se le pasa por parametro. Tiene que
     * tener la estructura:
     * Código;NIF;Apellidos;Nombre;Domicilio;CódigoPostal;Localidad;Teléfono;Móvil;Fax;Email
     *
     * @param datos String en el que se almacenan los datos
     * @throws java.lang.Exception Se lanzan las excepciones para controlarlas
     * desde la interfaz
     */
    public void guardarDatosCliente(String datos) throws Exception {
        String[] datosSeparados = datos.split(";");
        String sqlGuardar = "INSERT INTO Clientes VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement psGuardar;
        psGuardar = con.prepareStatement(sqlGuardar);

        for (int i = 1; i <= 12; i++) {
            psGuardar.setString(i, datosSeparados[i - 1]);
        }

        psGuardar.executeUpdate();

        psGuardar.close();
    }

    /**
     * Borra el elemento de la tabla
     *
     * @param codigo String que contiene el codigo del elemento que se desea
     * cerrar
     * @throws java.lang.Exception Se lanzan las excepciones para controlarlas
     * desde la interfaz
     */
    public void eliminarDatosCliente(String codigo) throws Exception {
        String sqlBorrar = "DELETE FROM Clientes WHERE codigo = '" + codigo + "'";
        stBusqueda.executeUpdate(sqlBorrar);
    }

    /**
     * Modifica un elemento de la tabla. Los datos tienen que tener el formato:
     * Código;NIF;Apellidos;Nombre;Domicilio;CódigoPostal;Localidad;Teléfono;Móvil;Fax;Email
     *
     * @param datos String que contiene los datos modificados
     * @throws java.lang.Exception Se lanzan las excepciones para controlarlas
     * desde la interfaz
     */
    public void modificarDatosCliente(String datos) throws Exception {
        String sqlModificar = "UPDATE Clientes SET nif=?,apellidos=?,nombre=?,"
                + "domicilio=?,codigo_postal=?,localidad=?,telefono=?,movil=?,"
                + "fax=?,email=?,total_ventas=? WHERE codigo = ?";
        String[] datosSeparados = datos.split(";");
        PreparedStatement psGuardar;
        psGuardar = con.prepareStatement(sqlModificar);

        for (int i = 1; i <= 12; i++) {
            if (i != 12) {
                psGuardar.setString(i, datosSeparados[i]);
            } else {
                psGuardar.setString(12, datosSeparados[0]);
            }
        }

        psGuardar.executeUpdate();
        psGuardar.close();
    }

    /**
     * Lista el nombre de columnas que se usaran para la tabla
     *
     * @return String [] Contiene los nombres
     */
    public String[] listarNombreColumnasArticulo() {
        String[] nombres = {"Código", "Descripción", "Stock", "Precio"};
        return nombres;
    }

    /**
     * Realiza una consulta de la tabla Articulos
     *
     * @param codigo String con el codigo que se desea consultar
     * @return Articulo contiene los datos del articulo
     * @throws Exception
     */
    public Articulo consultarArticulo(String codigo) throws Exception {

        String sql = "SELECT * FROM Articulos WHERE codigo='" + codigo + "'";
        Articulo resultado = null;

        ResultSet rs = stBusqueda.executeQuery(sql);

        if (rs.next()) {
            resultado = new Articulo(rs.getString("codigo"), rs.getString("descripcion"),
                    rs.getDouble("stock"), rs.getDouble("stock_minimo"), rs.getDouble("precio_compra"), rs.getDouble("precio_venta"));
        }

        return resultado;
    }

    /**
     * Modifica el stock de un articulo
     *
     * @param codigo String con el codigo del articulo
     * @param nuevoStock double con el nuevo valor
     * @throws Exception
     */
    public void actualizarStock(String codigo, double nuevoStock) throws Exception {
        String sqlModificar = "Update Articulos SET stock=" + nuevoStock + " Where codigo='" + codigo + "'";
        stBusqueda.executeUpdate(sqlModificar);
    }

    /**
     * Modifica el total_ventas de un cliente/proveedor
     *
     * @param codigo String con el codigo del cliente/proveedor
     * @param nuevoTotal double con el nuevo valor
     * @param esCliente boolean que indica si es cliente o preveedor
     * @throws Exception
     */
    public void actualizarTotalVentas(String codigo, double nuevoTotal, boolean esCliente) throws Exception {

        String tabla;
        if (esCliente) {
            tabla = "Clientes";
        } else {
            tabla = "Proveedores";
        }

        String sqlModificar = "Update " + tabla + " SET total_ventas=" + nuevoTotal + " Where codigo='" + codigo + "'";
        stBusqueda.executeUpdate(sqlModificar);
    }

    /**
     * Guarda todas las ventas del carrito
     * @param carrito Carrito contiene todas las ventas web que se desena guardar.
     * @throws Exception 
     */
    public void guardarCarrito(Carrito carrito) throws Exception{
        for (int i = 0; i < carrito.getCarrito().size(); i++) {
            guardarVentaWeb(carrito.getCarrito().get(i));
        }
    }
    
    /**
     * Guarda una venta web en la BBDD
     * @param ventaWeb VentaWeb con la venta que se desea guardar
     * @throws Exception 
     */
    public void guardarVentaWeb(VentaWeb ventaWeb) throws Exception{
        String sqlGuardar = "INSERT INTO ventasWeb VALUES(?,?,?,?)";
        
        PreparedStatement psGuardar;
        psGuardar = con.prepareStatement(sqlGuardar);

        psGuardar.setString(1, ventaWeb.getCliente().getCodigo());
        psGuardar.setString(2, ventaWeb.getArticulo().getCodigo());
        psGuardar.setFloat(3, ventaWeb.getUnidades());
        psGuardar.setDate(4, ventaWeb.getFecha());

        psGuardar.executeUpdate();

        psGuardar.close();
    }
    
    public Carrito buscarVentas(Cliente cliente, Date fechaInicio, Date fechaFinal) throws Exception{
        
        String sqlBusqueda = "SELECT * FROM ventasWeb WHERE clientes=? AND CAST(fecha AS DATE) BETWEEN '"+fechaInicio.toString()+"' AND '"+fechaFinal.toString()+"'";
        
        PreparedStatement psBuscar;
        psBuscar = con.prepareCall(sqlBusqueda);
        psBuscar.setString(1, cliente.getCodigo());
        
        
        
        ResultSet rs = psBuscar.executeQuery();
        Carrito carrito = new Carrito();
        while (rs.next()) {           
            carrito.guardarVentaWeb(new VentaWeb(cliente, consultarArticulo(rs.getString("articulo")), rs.getFloat("unidades"), rs.getDate("fecha")));
        }
        
        return carrito;
    }
    
}
