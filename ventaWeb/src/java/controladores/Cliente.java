/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

/**
 *
 * @author dany-
 */
public class Cliente {

    private String codigo;
    private String nif;
    private String apellido;
    private String nombre;
    private String domicilio;
    private String codigo_postal;
    private String localidad;
    private String telefono;
    private String movil;
    private String fax;
    private String email;
    private double total_ventas;

    /**
     * Contructor que inicializa un cliente vacio
     */
    public Cliente() {
        this.codigo = "";
        this.nif = "";
        this.apellido = "";
        this.nombre = "";
        this.domicilio = "";
        this.codigo_postal = "";
        this.localidad = "";
        this.telefono = "";
        this.movil = "";
        this.fax = "";
        this.email = "";
        this.total_ventas = 0;
    }

    public Cliente(String codigo) {
        String textoFinal = "";
        for (int i = 0; i < 6 - codigo.length(); i++) {
            textoFinal += "0";
        }
        textoFinal += codigo;
        this.codigo = textoFinal;
    }

    public Cliente(String codigo, String nif, String apellido, String nombre, String domicilio, String codigo_postal, String localidad, String telefono, String movil, String fax, String email, double total_ventas) {
        this.codigo = codigo;
        this.nif = nif;
        this.apellido = apellido;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.codigo_postal = codigo_postal;
        this.localidad = localidad;
        this.telefono = telefono;
        this.movil = movil;
        this.fax = fax;
        this.email = email;
        this.total_ventas = total_ventas;
    }

    @Override
    public String toString() {
        return codigo + ";" + nif + ";" + apellido + ";" + nombre + ";" + domicilio + ";" + codigo_postal + ";" + localidad + ";"
                + telefono + ";" + movil + ";" + fax + ";" + email + ";" + total_ventas;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getCodigo_postal() {
        return codigo_postal;
    }

    public void setCodigo_postal(String codigo_postal) {
        this.codigo_postal = codigo_postal;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getTotal_ventas() {
        return total_ventas;
    }

    public void setTotal_ventas(double total_ventas) {
        this.total_ventas = total_ventas;
    }

}
