/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

/**
 *
 * @author dany-
 */
public class Carrito {

    private final ArrayList<VentaWeb> carrito;

    public ArrayList<VentaWeb> getCarrito() {
        return carrito;
    }

    public Carrito() {
        this.carrito = new ArrayList<VentaWeb>();
    }

    public void vaciarCarrito() {
        this.carrito.clear();
    }

    public void guardarVentaWeb(VentaWeb ventaWeb) {
        carrito.add(ventaWeb);
    }

    public int numeroVentas() {
        return carrito.size();
    }

    public VentaWeb ventaWeb(int i) {
        return carrito.get(i);
    }

    public String costeTotal(int posicionPedido) {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
        otherSymbols.setDecimalSeparator('.');
        DecimalFormat f = new DecimalFormat("#0.00", otherSymbols);
        return f.format(carrito.get(posicionPedido).getUnidades() * carrito.get(posicionPedido).getArticulo().getPrecio_venta());
    }

    public String precio(int posicionPedido) {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
        otherSymbols.setDecimalSeparator('.');
        DecimalFormat f = new DecimalFormat("#0.00", otherSymbols);
        return f.format(carrito.get(posicionPedido).getArticulo().getPrecio_venta());
    }

    public String precioTotal() {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
        otherSymbols.setDecimalSeparator('.');
        DecimalFormat f = new DecimalFormat("#0.00", otherSymbols);

        double precio = 0;

        for (int i = 0; i < carrito.size(); i++) {

            precio += Double.valueOf(costeTotal(i));

        }

        return f.format(precio);
    }
}
