/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.util.TreeMap;

/**
 *
 * @author dany-
 */
public class Articulo {

    private String codigo;
    private String descripcion;
    private double stock;
    private double stock_minimo;
    private double precio_compra;
    private double precio_venta;

    private boolean codigoModificado;
    private boolean descripcionModificado;
    private boolean stockModificado;
    private boolean stock_minimoModificado;
    private boolean precio_compraModificado;
    private boolean precio_ventaModificado;

    public Articulo() {
        this.codigo = "";
        this.descripcion = "";
        this.stock = 0;
        this.stock_minimo = 0;
        this.precio_compra = 0;
        this.precio_venta = 0;

    }

    public Articulo(String codigo, String descripcion, double stock, double stock_minimo, double precio_compra, double precio_venta) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.stock = stock;
        this.stock_minimo = stock_minimo;
        this.precio_compra = precio_compra;
        this.precio_venta = precio_venta;

        codigoModificado = false;
        descripcionModificado = false;
        stockModificado = false;
        stock_minimoModificado = false;
        precio_compraModificado = false;
        precio_ventaModificado = false;

    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
        codigoModificado = true;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
        descripcionModificado = true;
    }

    public double getStock() {
        return stock;
    }

    public void setStock(double stock) {
        this.stock = stock;
        stockModificado = true;
    }

    public double getStock_minimo() {
        return stock_minimo;
    }

    public void setStock_minimo(double stock_minimo) {
        this.stock_minimo = stock_minimo;
        stock_minimoModificado = true;
    }

    public double getPrecio_compra() {
        return precio_compra;
    }

    public void setPrecio_compra(double precio_compra) {
        this.precio_compra = precio_compra;
        precio_compraModificado = true;
    }

    public double getPrecio_venta() {
        return precio_venta;
    }

    public void setPrecio_venta(double precio_venta) {
        this.precio_venta = precio_venta;
        precio_ventaModificado = true;
    }

}
