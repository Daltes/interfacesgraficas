/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menus;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

/**
 *
 * @author alumno
 */
public class InterfazPrincipal extends javax.swing.JFrame {

    /**
     * Creates new form InterfazPrincipal
     */
    public InterfazPrincipal() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        jButtonImpresora = new javax.swing.JButton();
        jButtonEmail = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButtonInformacion = new javax.swing.JButton();
        jMenuBarPrincipal = new javax.swing.JMenuBar();
        jMenuArchivo = new javax.swing.JMenu();
        jMenuEnviar = new javax.swing.JMenu();
        jMenuItemImpresora = new javax.swing.JMenuItem();
        jMenuItemEmail = new javax.swing.JMenuItem();
        jMenuItemSalir = new javax.swing.JMenuItem();
        jMenuAcercaDe = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jToolBar1.setRollover(true);

        jButtonImpresora.setIcon(new javax.swing.ImageIcon(getClass().getResource("/printer.png"))); // NOI18N
        jButtonImpresora.setFocusable(false);
        jButtonImpresora.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonImpresora.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonImpresora.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonImpresoraActionPerformed(evt);
            }
        });
        jToolBar1.add(jButtonImpresora);

        jButtonEmail.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mail-unread.png"))); // NOI18N
        jButtonEmail.setToolTipText("");
        jButtonEmail.setFocusable(false);
        jButtonEmail.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonEmail.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEmailActionPerformed(evt);
            }
        });
        jToolBar1.add(jButtonEmail);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/edit-delete.png"))); // NOI18N
        jButton3.setFocusable(false);
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton3);

        jButtonInformacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/edit-copy.png"))); // NOI18N
        jButtonInformacion.setFocusable(false);
        jButtonInformacion.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonInformacion.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonInformacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonInformacionActionPerformed(evt);
            }
        });
        jToolBar1.add(jButtonInformacion);

        jMenuArchivo.setMnemonic('a');
        jMenuArchivo.setText("Archivo");

        jMenuEnviar.setMnemonic('e');
        jMenuEnviar.setText("Enviar");

        jMenuItemImpresora.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemImpresora.setMnemonic('i');
        jMenuItemImpresora.setText("Impresora");
        jMenuItemImpresora.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemImpresoraActionPerformed(evt);
            }
        });
        jMenuEnviar.add(jMenuItemImpresora);

        jMenuItemEmail.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemEmail.setMnemonic('e');
        jMenuItemEmail.setText("Email");
        jMenuItemEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemEmailActionPerformed(evt);
            }
        });
        jMenuEnviar.add(jMenuItemEmail);

        jMenuArchivo.add(jMenuEnviar);

        jMenuItemSalir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemSalir.setMnemonic('s');
        jMenuItemSalir.setText("Salir");
        jMenuItemSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemSalirActionPerformed(evt);
            }
        });
        jMenuArchivo.add(jMenuItemSalir);

        jMenuBarPrincipal.add(jMenuArchivo);

        jMenuAcercaDe.setMnemonic('d');
        jMenuAcercaDe.setText("Acerca de");
        jMenuAcercaDe.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jMenuAcercaDeFocusGained(evt);
            }
        });
        jMenuAcercaDe.addMenuListener(new javax.swing.event.MenuListener() {
            public void menuSelected(javax.swing.event.MenuEvent evt) {
                jMenuAcercaDeMenuSelected(evt);
            }
            public void menuDeselected(javax.swing.event.MenuEvent evt) {
            }
            public void menuCanceled(javax.swing.event.MenuEvent evt) {
            }
        });
        jMenuAcercaDe.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenuAcercaDeMouseClicked(evt);
            }
        });
        jMenuBarPrincipal.add(jMenuAcercaDe);

        setJMenuBar(jMenuBarPrincipal);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 241, Short.MAX_VALUE)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemImpresoraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemImpresoraActionPerformed
        notificar(this, "Notificacion", jMenuItemImpresora);
    }//GEN-LAST:event_jMenuItemImpresoraActionPerformed

    private void jMenuItemEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemEmailActionPerformed
        notificar(this, "Notificacion", jMenuItemEmail);
    }//GEN-LAST:event_jMenuItemEmailActionPerformed

    private void jMenuItemSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemSalirActionPerformed
        System.exit(-1);
    }//GEN-LAST:event_jMenuItemSalirActionPerformed

    private void jButtonImpresoraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonImpresoraActionPerformed
        notificar(this, "Notificacion", jMenuItemImpresora);
    }//GEN-LAST:event_jButtonImpresoraActionPerformed

    private void jButtonEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEmailActionPerformed
        notificar(this, "Notificacion", jMenuItemEmail);
    }//GEN-LAST:event_jButtonEmailActionPerformed

    private void jButtonInformacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonInformacionActionPerformed
        notificar(this, "Notificacion", jMenuAcercaDe);
    }//GEN-LAST:event_jButtonInformacionActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        System.exit(-1);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jMenuAcercaDeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuAcercaDeMouseClicked
        // Se crea el String que contendrá el mensaje final del error
        String contenido = "";
        // Se comprueba si se ha introducido un mensaje
        contenido = "Ha pulsado Acerca de";
        // Se muestra el pop up con el mensaje final y el icono de WARNIGN_MESSAGE
        JOptionPane.showMessageDialog(this, contenido, "Notificacion", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jMenuAcercaDeMouseClicked

    private void jMenuAcercaDeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jMenuAcercaDeFocusGained
        String contenido = "";
        // Se comprueba si se ha introducido un mensaje
        contenido = "Ha pulsado Acerca de";
        // Se muestra el pop up con el mensaje final y el icono de WARNIGN_MESSAGE
        JOptionPane.showMessageDialog(this, contenido, "Notificacion", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jMenuAcercaDeFocusGained

    private void jMenuAcercaDeMenuSelected(javax.swing.event.MenuEvent evt) {//GEN-FIRST:event_jMenuAcercaDeMenuSelected
        String contenido = "";
        // Se comprueba si se ha introducido un mensaje
        contenido = "Ha pulsado Acerca de";
        // Se muestra el pop up con el mensaje final y el icono de WARNIGN_MESSAGE
        JOptionPane.showMessageDialog(this, contenido, "Notificacion", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jMenuAcercaDeMenuSelected

    /**
     * Lanza un mensaje de pop up con una notificacion
     *
     * @param padre JFrame en el que se centrará la ventana
     * @param titulo String que contiene el titulo de la ventana
     * @param mensaje Mensaje que se quiere dar en la ventana
     */
    public static void notificar(JFrame padre, String titulo, JMenuItem item) {
        // Se crea el String que contendrá el mensaje final del error
        String contenido = "";
        // Se comprueba si se ha introducido un mensaje
        contenido = "Ha pulsado " + item.getText();
        // Se muestra el pop up con el mensaje final y el icono de WARNIGN_MESSAGE
        JOptionPane.showMessageDialog(padre, contenido, titulo, JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InterfazPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InterfazPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InterfazPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InterfazPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InterfazPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButtonEmail;
    private javax.swing.JButton jButtonImpresora;
    private javax.swing.JButton jButtonInformacion;
    private javax.swing.JMenu jMenuAcercaDe;
    private javax.swing.JMenu jMenuArchivo;
    private javax.swing.JMenuBar jMenuBarPrincipal;
    private javax.swing.JMenu jMenuEnviar;
    private javax.swing.JMenuItem jMenuItemEmail;
    private javax.swing.JMenuItem jMenuItemImpresora;
    private javax.swing.JMenuItem jMenuItemSalir;
    private javax.swing.JToolBar jToolBar1;
    // End of variables declaration//GEN-END:variables
}
